\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{15}
\contentsline {section}{\numberline {1.1}Introduction}{15}
\contentsline {section}{\numberline {1.2}Rappels}{16}
\contentsline {subsection}{\numberline {1.2.1}Les lois de Newton}{16}
\contentsline {subsubsection}{Premi\IeC {\`e}re loi (loi d'inertie)}{16}
\contentsline {subsubsection}{Deuxi\IeC {\`e}me loi (principe fondamental)}{16}
\contentsline {subsubsection}{Troisi\IeC {\`e}me loi (action et r\IeC {\'e}action)}{16}
\contentsline {subsection}{\numberline {1.2.2}La loi de la gravitation universelle}{16}
\contentsline {subsection}{\numberline {1.2.3}Conclusion}{16}
\contentsline {chapter}{\numberline {2}\IeC {\'E}lectrostatique }{17}
\contentsline {section}{\numberline {2.1}Introduction}{17}
\contentsline {section}{\numberline {2.2}La loi de Coulomb}{17}
\contentsline {subsection}{\numberline {2.2.1}La charge \IeC {\'e}lectrique}{17}
\contentsline {subsection}{\numberline {2.2.2}Le pendule \IeC {\'e}lectrique}{17}
\contentsline {subsubsection}{Description de l'exp\IeC {\'e}rience}{17}
\contentsline {subsubsection}{Explications}{17}
\contentsline {subsection}{\numberline {2.2.3}Les isolants}{19}
\contentsline {subsection}{\numberline {2.2.4}L'\IeC {\'e}lectroscope}{19}
\contentsline {subsubsection}{Description de l'exp\IeC {\'e}rience}{19}
\contentsline {subsubsection}{Explications}{19}
\contentsline {subsection}{\numberline {2.2.5}Charge par influence}{20}
\contentsline {subsubsection}{Description de l'exp\IeC {\'e}rience}{20}
\contentsline {subsubsection}{Explications}{20}
\contentsline {subsection}{\numberline {2.2.6}La machine de Van de Graaff}{20}
\contentsline {subsubsection}{Description de l'exp\IeC {\'e}rience}{20}
\contentsline {subsubsection}{Explications}{20}
\contentsline {subsection}{\numberline {2.2.7}La machine de Wimshurst}{21}
\contentsline {subsection}{\numberline {2.2.8}La foudre}{21}
\contentsline {subsection}{\numberline {2.2.9}Conservation et charge \IeC {\'e}l\IeC {\'e}mentaire}{23}
\contentsline {subsection}{\numberline {2.2.10}La loi de Coulomb}{23}
\contentsline {section}{\numberline {2.3}Le champ \IeC {\'e}lectrique}{24}
\contentsline {subsection}{\numberline {2.3.1}La loi de Coulomb}{24}
\contentsline {subsection}{\numberline {2.3.2}La notion de champ}{25}
\contentsline {subsection}{\numberline {2.3.3}D\IeC {\'e}finition}{26}
\contentsline {subsection}{\numberline {2.3.4}Corollaires}{27}
\contentsline {subsection}{\numberline {2.3.5}La cage de Faraday}{28}
\contentsline {subsection}{\numberline {2.3.6}Lignes de champ}{28}
\contentsline {subsection}{\numberline {2.3.7}La notion de potentiel}{28}
\contentsline {chapter}{\numberline {3}\IeC {\'E}lectrocin\IeC {\'e}tique}{31}
\contentsline {section}{\numberline {3.1}Circuit et courant}{31}
\contentsline {section}{\numberline {3.2}R\IeC {\'e}sistance et loi d'Ohm}{31}
\contentsline {section}{\numberline {3.3}S\IeC {\'e}curit\IeC {\'e} \IeC {\'e}lectrique}{33}
\contentsline {subsection}{\numberline {3.3.1}Introduction}{33}
\contentsline {subsection}{\numberline {3.3.2}L'\IeC {\'e}lectricit\IeC {\'e} et l'homme}{34}
\contentsline {subsubsection}{Facteurs importants}{35}
\contentsline {subsubsection}{Sympt\IeC {\^o}mes}{35}
\contentsline {subsubsection}{Traitements}{35}
\contentsline {subsection}{\numberline {3.3.3}Les mesures de s\IeC {\'e}curit\IeC {\'e}}{35}
\contentsline {subsubsection}{Le r\IeC {\'e}seau \IeC {\'e}lectrique}{35}
\contentsline {subsubsection}{Pour les b\IeC {\^a}timents}{36}
\contentsline {subsubsection}{Pour l'homme}{37}
\contentsline {section}{\numberline {3.4}\IeC {\'E}nergie et puissance \IeC {\'e}lectrique}{39}
\contentsline {section}{\numberline {3.5}Accumulateurs}{40}
\contentsline {subsection}{\numberline {3.5.1}\IeC {\'E}quations}{40}
\contentsline {subsection}{\numberline {3.5.2}Types d'accumulateurs}{41}
\contentsline {section}{\numberline {3.6}Circuits \IeC {\'e}lectriques : les multiprises}{41}
\contentsline {subsection}{\numberline {3.6.1}Circuit s\IeC {\'e}rie}{42}
\contentsline {subsection}{\numberline {3.6.2}Circuit parall\IeC {\`e}le}{42}
\contentsline {chapter}{\numberline {4}Magn\IeC {\'e}tisme}{45}
\contentsline {section}{\numberline {4.1}Introduction}{45}
\contentsline {section}{\numberline {4.2}Le champ magn\IeC {\'e}tique}{45}
\contentsline {subsection}{\numberline {4.2.1}D\IeC {\'e}finitions}{45}
\contentsline {subsection}{\numberline {4.2.2}Le champ magn\IeC {\'e}tique terrestre}{46}
\contentsline {subsection}{\numberline {4.2.3}L'exp\IeC {\'e}rience d'\OE rsted}{47}
\contentsline {subsection}{\numberline {4.2.4}Le magn\IeC {\'e}tisme dans la mati\IeC {\`e}re}{49}
\contentsline {subsection}{\numberline {4.2.5}Application}{51}
\contentsline {section}{\numberline {4.3}La loi de Laplace}{51}
\contentsline {subsection}{\numberline {4.3.1}D\IeC {\'e}finition}{51}
\contentsline {subsection}{\numberline {4.3.2}Applications}{51}
\contentsline {subsubsection}{Galvanom\IeC {\`e}tre}{53}
\contentsline {subsubsection}{Moteur \IeC {\'e}lectrique}{53}
\contentsline {section}{\numberline {4.4}La loi de Lorentz}{54}
\contentsline {subsection}{\numberline {4.4.1}D\IeC {\'e}finition}{54}
\contentsline {subsection}{\numberline {4.4.2}Applications}{55}
\contentsline {subsubsection}{T\IeC {\'e}l\IeC {\'e}vision}{55}
\contentsline {subsubsection}{Acc\IeC {\'e}l\IeC {\'e}rateur de particules : le cyclotron}{56}
\contentsline {subsubsection}{Spectrographe de masse}{57}
\contentsline {chapter}{\numberline {5}\IeC {\'E}lectromagn\IeC {\'e}tisme}{59}
\contentsline {section}{\numberline {5.1}Nature de la lumi\IeC {\`e}re}{59}
\contentsline {section}{\numberline {5.2}Applications}{60}
\contentsline {chapter}{\numberline {A}Unit\IeC {\'e}s internationales}{67}
\contentsline {section}{\numberline {A.1}Introduction}{67}
\contentsline {section}{\numberline {A.2}Les unit\IeC {\'e}s choisies}{67}
\contentsline {section}{\numberline {A.3}Exemple}{68}
\contentsline {section}{\numberline {A.4}Conversions}{68}
\contentsline {section}{\numberline {A.5}Multiples et sous-multiples}{68}
\contentsline {section}{\numberline {A.6}Notation scientifique}{68}
\contentsline {section}{\numberline {A.7}Valeurs importantes}{69}
\contentsline {chapter}{\numberline {B}Petite histoire de l'\IeC {\'e}lectricit\IeC {\'e}}{71}
\contentsline {section}{\numberline {B.1}Introduction}{71}
\contentsline {section}{\numberline {B.2}Le galvanisme}{71}
\contentsline {section}{\numberline {B.3}Frankenstein}{73}
\contentsline {section}{\numberline {B.4}La pile de Volta}{74}
\contentsline {section}{\numberline {B.5}Courant et tension : Amp\IeC {\`e}re}{76}
\contentsline {section}{\numberline {B.6}Ohm et la notion de r\IeC {\'e}sistance}{79}
\contentsline {section}{\numberline {B.7}Conclusion}{79}
\contentsline {chapter}{\numberline {C}La pile \IeC {\'e}lectrique}{81}
\contentsline {chapter}{\numberline {D}La notion de tension}{85}
\contentsline {section}{\numberline {D.1}Analogie gravitationnelle}{85}
\contentsline {section}{\numberline {D.2}Tension et potentiel \IeC {\'e}lectrique}{85}
\contentsline {chapter}{\numberline {E}Circuits en s\IeC {\'e}rie et en parall\IeC {\`e}le}{87}
\contentsline {section}{\numberline {E.1}Deux r\IeC {\'e}sistances en s\IeC {\'e}rie}{87}
\contentsline {section}{\numberline {E.2}Deux r\IeC {\'e}sistances en parall\IeC {\`e}les}{87}
\contentsline {section}{\numberline {E.3}Circuit quelconque}{88}
\contentsline {chapter}{\numberline {F}Vitesse des \IeC {\'e}lectrons dans un fil conducteur}{89}
\contentsline {chapter}{\numberline {G}Ligne \IeC {\`a} haute tension}{91}
\contentsline {chapter}{\numberline {H}Le cyclotron en \IeC {\'e}quations}{95}
\contentsline {chapter}{\numberline {I}Exercices}{97}
\contentsline {section}{\numberline {I.1}Probl\IeC {\`e}mes}{97}
\contentsline {subsection}{\numberline {I.1.1}Relatifs \IeC {\`a} la loi de Coulomb}{97}
\contentsline {subsection}{\numberline {I.1.2}Relatifs au champ \IeC {\'e}lectrique}{98}
\contentsline {subsection}{\numberline {I.1.3}Relatifs \IeC {\`a} la s\IeC {\'e}curit\IeC {\'e} \IeC {\'e}lectrique}{98}
\contentsline {subsection}{\numberline {I.1.4}Relatifs aux loi d'Ohm et de Pouillet, \IeC {\`a} la puissance et \IeC {\`a} l'\IeC {\'e}nergie}{99}
\contentsline {subsection}{\numberline {I.1.5}Relatifs au magn\IeC {\'e}tisme}{100}
\contentsline {section}{\numberline {I.2}Solutions}{102}
